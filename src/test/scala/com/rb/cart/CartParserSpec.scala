package com.rb.cart

import com.rb.BaseSpec

class CartParserSpec extends BaseSpec {

  "Given a valid Json CartParser" should "return Cart" in {
    val jsonStr = """[
                    |  {
                    |    "product-type": "hoodie",
                    |    "options": {
                    |      "size": "small",
                    |      "colour": "white",
                    |      "print-location": "front"
                    |    },
                    |    "artist-markup": 20,
                    |    "quantity": 1
                    |  }
                    |]""".stripMargin

    val cart = CartParser(jsonStr).right.get
    cart.items.length should be (1)
    inside (cart.items(0)) {
      case CartItem(product, options, markup, quantity) =>
        product should be ("hoodie")
        options should equal (Map("size" -> "small", "colour" -> "white", "print-location" -> "front"))
        markup should be (20.0)
        quantity should be (1)
    }
  }
}
