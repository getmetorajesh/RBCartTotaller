package com.rb

class IntegrationSpec extends BaseSpec {

  info("Read json")
  "Given a valid file path for product and base price" should  "return json string" in {
    val cartFile = getClass.getResource("/cart-4560.json").getPath
    val basePriceFile = getClass.getResource("/base-prices.json").getPath
    val rawInputs = RawInputs(cartFile, basePriceFile)

    val res = Main.readFiles(rawInputs)
    res should be ('right)
  }

  info("Parse json")
  "Given a valid json strings for product and base price" should  "return Cart and Product Prices" in {
    val cartFile = getClass.getResource("/cart-4560.json").getPath
    val basePriceFile = getClass.getResource("/base-prices.json").getPath
    val rawInputs = RawInputs(cartFile, basePriceFile)
    val res = Main.readFiles(rawInputs)

    val cartProductPriceE = res.map(Main.parseJson)
    cartProductPriceE should be ('right)
  }




}
