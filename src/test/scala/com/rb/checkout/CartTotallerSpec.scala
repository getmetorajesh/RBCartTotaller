package com.rb.checkout

import com.rb.BaseSpec
import com.rb.itemprice.ItemPriceCalculator

class CartTotallerSpec extends BaseSpec {

  def calculateTotalFor(file: String): Int = {
    val (cart, prices) = cartPriceFixture(file)
    val itemizedE = ItemPriceCalculator.apply((cart, prices))
    val itemized = itemizedE.right.get
    val cartTotalE = CartTotaller.apply(itemized)
    cartTotalE.right.get.totalInCents
  }

  "Given List of ItemPrices to the cart totaller" should "return non error" in {
    val (cart, prices) = cartPriceFixture(cartFile_4)
    val itemizedE = ItemPriceCalculator.apply((cart, prices))
    val itemized = itemizedE.right.get
    val cartTotalE = CartTotaller.apply(itemized)

    cartTotalE should be('right)
  }

  "Given List of ItemPrices to the cart totaller" should "return total value in cents" in {
    calculateTotalFor(cartFile_1) should be (4560)
    calculateTotalFor(cartFile_2) should be (9363)
    calculateTotalFor(cartFile_3) should be (9500)
    calculateTotalFor(cartFile_4) should be (11356)
  }

}
