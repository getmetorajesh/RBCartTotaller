package com.rb

import org.scalatest.{FlatSpec, Inside, Matchers, OptionValues}

abstract class BaseSpec extends FlatSpec with Matchers with
  OptionValues with Inside {
  val (cartFile_1, cartFile_2, cartFile_3, cartFile_4) = ("/cart-4560.json", "/cart-9363.json", "/cart-9500.json", "/cart-11356.json")

  def cartPriceFixture(file: String) = {
    val cartFile = getClass.getResource(file).getPath
    val basePriceFile = getClass.getResource("/base-prices.json").getPath
    val rawInputs = RawInputs(cartFile, basePriceFile)
    val res = Main.readFiles(rawInputs).right
    val (cart, prices) = Main.parseJson(res.get).right.get
    (cart, prices)
  }
}

