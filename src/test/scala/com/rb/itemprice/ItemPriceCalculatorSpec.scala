package com.rb.itemprice

import com.rb.BaseSpec

class ItemPriceCalculatorSpec extends BaseSpec {

  "Given a valid Cart and Product Prices" should "return List of Itemised Price" in {
    val (cart, prices) = cartPriceFixture(cartFile_1)
    val itemizedE = ItemPriceCalculator.apply((cart, prices))
    itemizedE should be ('right)
    val itemized = itemizedE.right.get

    inside(itemized.head) {
      case ProductPrice(product, priceInCent) =>
        product should be ("hoodie")
        priceInCent should be (4560)
    }
  }

  "Given a valid Cart(2 items) and Product Prices" should "return List of 2" in {
    val (cart, prices) = cartPriceFixture(cartFile_2)
    val itemizedE = ItemPriceCalculator.apply((cart, prices))
    itemizedE should be ('right)
    val itemized = itemizedE.right.get

    itemized.length should be (2)
  }
}
