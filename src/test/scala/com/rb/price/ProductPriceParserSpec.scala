package com.rb.price

import com.rb.BaseSpec

class ProductPriceParserSpec extends BaseSpec {

  "Given a valid Json ProductPriceParser" should "return valid ProductPrice" in {
    val jsonStr = """[{
                    |    "product-type": "sticker",
                    |    "options": {
                    |      "size": ["large"]
                    |    },
                    |    "base-price": 1000
                    |  },
                    |  {
                    |    "product-type": "sticker",
                    |    "options": {
                    |    },
                    |    "base-price": 1000
                    |  }]""".stripMargin


    val catalogE = ProductPriceParser(jsonStr)
    catalogE should be ('right)

    val catalog = catalogE.right.get
    catalog.products.length should be (2)
    inside (catalog.products.head) {
      case ProductBase(product, options, price) =>
        product should be ("sticker")
        options should equal (Map("size" -> List("large")))
        price should be (1000)
    }

    inside (catalog.products(1)) {
      case ProductBase(product, options, price) =>
        product should be ("sticker")
        options should equal (Map())
        price should be (1000)
    }
  }
}
