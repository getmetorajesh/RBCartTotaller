package com.rb.cart

import io.circe.{Decoder, HCursor}
import io.circe.parser._
import com.rb.utils.Errors._

/**
  * Cart Parser decodes json to Cart case class
  */
object CartParser {
  implicit val decodeCartItem: Decoder[CartItem] = new Decoder[CartItem] {
    final def apply(c: HCursor): Decoder.Result[CartItem] =
      for {
        productType <- c.downField("product-type").as[String]
        options <- c.downField("options").as[Map[String, String]]
        artistMarkup <- c.downField("artist-markup").as[Int]
        quantity <- c.downField("quantity").as[Int]
      } yield {
        new CartItem(productType, options, artistMarkup, quantity)
      }
  }

  implicit val decodeCart: Decoder[Cart] = new Decoder[Cart] {
    final def apply(c: HCursor): Decoder.Result[Cart] =
      for {
        items <- c.as[List[CartItem]]
      } yield {
        new Cart(items)
      }
  }

  def apply(jsonStr: String): ErrorOr[Cart] = {
    decode[Cart](jsonStr) match {
      case Right(cart) => toSuccess(cart)
      case Left(err) => toFailure(err.toString())
    }
  }
}
