package com.rb.cart

case class CartItem(
                    productType: String,
                    options: Map[String, String],
                    artistMarkup: Double,
                    quantity: Double
                    )