package com.rb.price

case class ProductBase(
    productType: String,
    options: Map[String, List[String]],
    basePrice: Double
  )
