package com.rb.price

import com.rb.utils.Errors.{ErrorOr, toFailure, toSuccess}
import io.circe.{Decoder, HCursor}
import io.circe.parser.decode

/**
  * ProductPriceParser decodes JSON to ProductPrices case class
  */
object ProductPriceParser {
  implicit val decodeProductBase: Decoder[ProductBase] = new Decoder[ProductBase] {
    final def apply(c: HCursor): Decoder.Result[ProductBase] =
      for {
        productType <- c.downField("product-type").as[String]
        options <- c.downField("options").as[Map[String, List[String]]]
        basePrice <- c.downField("base-price").as[Int]
      } yield {
        ProductBase(productType, options, basePrice)
      }
  }

  implicit val decodePrices: Decoder[Catalog] = new Decoder[Catalog] {
    final def apply(c: HCursor): Decoder.Result[Catalog] =
      for {
        prices <- c.as[List[ProductBase]]
      } yield {
        Catalog(prices)
      }
  }

  def apply(jsonStr: String): ErrorOr[Catalog] = {
    decode[Catalog](jsonStr) match {
      case Right(prices) => toSuccess(prices)
      case Left(err) => toFailure(err.toString)
    }
  }
}
