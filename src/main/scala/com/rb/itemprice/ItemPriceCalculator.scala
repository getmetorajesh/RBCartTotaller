package com.rb.itemprice

import com.rb.cart.{Cart, CartItem}
import com.rb.price.{ProductBase, Catalog}
import com.rb.utils.Errors._

/**
  * Item Price Calculator calculated item level prices
  */
object ItemPriceCalculator {
  val cents = 100

  def apply: ((Cart, Catalog)) => ErrorOr[List[ProductPrice]] = { cartCatalog => {
    val (cart, catalog) = cartCatalog
    val itemisedPrices = cart.items.map(item => {
      val basePrice = productFinder(item, catalog.products).map(_.basePrice)
      basePrice match {
        case Some(price) => Some(ProductPrice(item.productType, calculatePrice(price, item)))
        case None => None
      }
    })

    // Assumption: if the product type  then ignoring the value
    toSuccess(itemisedPrices.flatten)
  }
  }

  /**
    * ProductFinder find the base product by comparing different attr
    * @param item CartItem
    * @param productPrices List[ProductBase]
    * @return ProductBase
    */
  def productFinder(item: CartItem, productPrices: List[ProductBase]): Option[ProductBase] = {
    productPrices
      .filter(p => p.productType == item.productType)
      .find(p => {
        val product = p.options.map { case (k, v) => v.contains(item.options(k)) }
        !product.toList.contains(false) // all attr should match
      })
  }

  /**
    * Calculate item price
    * @param basePrice Double
    * @param item CartItem
    * @return
    */
  def calculatePrice(basePrice: Double, item: CartItem): Int = {
    ((basePrice + Math.round(basePrice * (item.artistMarkup / cents))) * item.quantity).toInt
  }
}