package com.rb.itemprice

case class ProductPrice(product: String, priceInCents: Int)
