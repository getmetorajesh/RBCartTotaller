package com.rb.checkout

import com.rb.itemprice.ProductPrice
import com.rb.utils.Errors._

object CartTotaller {
  def apply: List[ProductPrice] => ErrorOr[CartTotal] = { cartItemPrices =>
    val total = cartItemPrices.map(_.priceInCents).sum
    toSuccess(CartTotal(total))
  }

}
