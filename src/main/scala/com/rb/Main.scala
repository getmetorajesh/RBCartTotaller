package com.rb

import cats.implicits._
import com.rb.cart.{Cart, CartParser}
import com.rb.utils.Errors._
import com.rb.utils.ReadFile
import cats.data.Kleisli
import com.rb.checkout.{CartTotal, CartTotaller}
import com.rb.itemprice.ItemPriceCalculator
import com.rb.price.{Catalog, ProductPriceParser}

case class CartProductJson(cartJson: String, productJson: String)
case class RawInputs(cartLocation: String, basePriceLocation: String)
object Main extends App {
  val help = s"""
**** RedBubble Cart Totaller ****
# Run with full path name to cart and base price json files eg:
> run /cart.json /base-price.json
****************************************
"""
  println(help)
  commandListener(args)

  /**
    * CommandListener is the main execution fn takes input args and prints the result
    * @param args Array[String]
    */
  def commandListener(args: Array[String]): Unit = {
    val graph: Array[String] => ErrorOr[Unit] =
      (Kleisli(commandParser) andThen
        readFiles andThen
          parseJson andThen
            ItemPriceCalculator.apply andThen
              CartTotaller.apply andThen
                printTotalPrice).run

    graph(args)
  }

  /**
    * ReadFiles transforms RawInputs to Json
    * @param RawInputs
    * @return CartAndProductJson
    */
  def readFiles: RawInputs => ErrorOr[CartProductJson] = { paths =>
    val jsonStrings = (ReadFile(paths.cartLocation), ReadFile(paths.basePriceLocation)) match {
      case (Right(s1), Right(s2)) => toSuccess(CartProductJson(s1, s2))
      case _ => toFailure("Can't read file. Check file location.")
    }
    jsonStrings
  }


  /**
    * ParseJson to Cart and ProductPrice case class
    * @param CartProductJson CartProductJson
    * @return ErrorOr[(Cart, ProductPrices)]
    */
  def parseJson: CartProductJson => ErrorOr[(Cart, Catalog)] = { cartProductJson =>
    val cart = CartParser(cartProductJson.cartJson)
    val product = ProductPriceParser(cartProductJson.productJson)
    (cart, product) match {
      case (Right(c), Right(p)) => toSuccess((c, p))
      case _ => toFailure("Failed to parse json")
    }
  }


  /**
    * Print total price
    * @return
    */
  def printTotalPrice: CartTotal => ErrorOr[Unit] = { cartSummary =>
    println(cartSummary.totalInCents)
    toSuccess(Unit)
  }

  def commandParser(args: Array[String]): ErrorOr[RawInputs] = {
    args.length match {
      case 2 => toSuccess(RawInputs(args(0), args(1)))
      case _ => toFailure("Invalid input")
    }
  }
}
