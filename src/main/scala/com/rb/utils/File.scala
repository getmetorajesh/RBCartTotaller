package com.rb.utils

import java.io.{FileInputStream, InputStream}
import Errors._

object ReadFile {
  def apply(path: String): ErrorOr[(String)] = {
    try {
      val stream: InputStream = new FileInputStream(path)
      val res = scala.io.Source.fromInputStream(stream).getLines.toList.mkString("\n")
      toSuccess(res)
    } catch {
      case _: Any => toFailure("Failed to parse")
    }
  }
}
